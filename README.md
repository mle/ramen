


Make sure the setting "Branch name templates" does not contradict your setting here

Settings that should be verified that : "Branch name template"

Other settings you should check to ensure that it follows

Please check the following settings for any potential conflicts: Branch name templates

Check "Branch defaults > Branch name templates" for potential conflicts

small


Ensure that the pattern does not contradict the push rule setting for Branch name.
Make sure the pattern does not contradict the Branch name setting under Push rules.


Make sure this rule here does not contradict your settings for the `Branch name` under `Push rules`.
Make sure this pattern does not contradict with the `Branch name` setting under `Push rules`.
Make sure this pattern follows with the `Branch name` setting under `Push rules`.
Make sure this pattern follows with the `Push rules > Branch name` setting.
Make sure this pattern does not contradict with the `Push rules > Branch name` setting.




Make sure the rule for Branch name templates under Branch defaults does not contradict your settings here.
Check `Branch name templates` under `Branch defaults` for potential conflicts.
Check `Branch defaults > Branch name templates` for potential conflicts.

Make sure this rule here does not contradict your settings for the `Branch name` under `Push rules`.
Make sure this pattern does not contradict the `Push rules > Branch name` setting.
Make sure this pattern follows the `Push rules > Branch name` setting.

Make sure the rule for Branch name templates under Branch defaults does not contradict your settings here.
Check `Branch defaults > Branch name templates` for potential conflicts.
